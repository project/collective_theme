'use strict';

!function (document, Drupal, $) {
  Drupal.behaviors.organizationInformation = {
    attach: function attach(context) {
      $('.organization-info__toggle', context).once().click(function () {
        if (parseInt($(window).width()) < 768) {
          // In the raw HTML output the container is the very next DOM item
          // after the toggle button, so this should be safe to do.
          var $infoContainer = $(this).next();
          var $toggleIcon = $('.organization-info__toggle-icon', this);
          var expandedState = $infoContainer.attr('aria-expanded') === 'true' ? 'false' : 'true';
          var hiddenState = $infoContainer.attr('aria-hidden') === 'false' ? 'false' : 'true';
          $infoContainer.attr('aria-expanded', expandedState);
          $infoContainer.attr('aria-hidden', hiddenState);
          $infoContainer.toggle();
          $toggleIcon.toggleClass('open closed');
        }
      });
    }
  };
}(document, Drupal, jQuery);
//# sourceMappingURL=organization-information.js.map
